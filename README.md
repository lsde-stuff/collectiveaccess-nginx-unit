# collectiveaccess-docker
## About
Dockerized CollectiveAccess using NGINX Unit webserver.
## Installation
### Using Docker Compose
    $ docker-compose up -d
### Using Helm
    $ kubectl create ns collectiveaccess
    $ helm install collectiveaccess ./chart --namespace collectiveaccess
## Configuration
### CollectiveAccess instance
#### under Docker Compose
    $ vim setup.php
#### under k8s
    $ kubectl edit configmap collectiveaccess-config --namespace collectiveaccess
### Unit webserver
#### under Docker Compose
    $ vim config.json
#### under k8s
    $ kubectl edit configmap webserver-config --namespace collectiveaccess