FROM unit:php

COPY config.json /docker-entrypoint.d/
RUN apt update && apt install -y zlib1g-dev libpng-dev libzip-dev libicu-dev mariadb-client && rm -rf /var/cache/apt
RUN docker-php-ext-install gd zip intl bcmath mysqli pdo pdo_mysql 

USER unit
WORKDIR /var/www/html
RUN curl -skL https://github.com/collectiveaccess/providence/archive/refs/heads/dev/php8.tar.gz -o - | tar xvzf - --strip-components=1
user root

EXPOSE 8080
